<footer class="content-info">
	<div class="container">
		<?php dynamic_sidebar('sidebar-footer'); ?>
	</div>
	<div class="container">
		<p>375 Queen Street, Melbourne, VIC 3000</p>
		<p>Copyright &copy; 2014 <a href="<?php echo get_site_url(); ?>">Cat Cafe Melbourne</a></p>
	</div>
</footer>
