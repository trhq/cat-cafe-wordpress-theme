<?php
wp_nav_menu( array(
    'menu'              => 'primary',
    'theme_location'    => 'primary',
    'depth'             => 2,
    'container'         => 'div',
    'container_class'   => 'collapse navbar-collapse',
    'container_id'      => 'bs-example-navbar-collapse-1',
    'menu_class'        => 'nav navbar-nav',
    'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
    'walker'            => new wp_bootstrap_navwalker())
);
?>
<header class="container" >
  <p style="float:left;">
    <a href="<?php echo site_url() ;?>" title="Cat Cafe Melbourne">
      <img src="<?php echo site_url() ;?>/files/Cat-Cafe-no-border-150.png">
    </a>
  </p>
  <p style="float:right; text-align: center" class="contact">
    Ph: (03) 9642 8540  <br>
    <a class="col-xs-3" title="Follow us on Facebook" href="https://www.facebook.com/pages/Cat-Cafe-Melbourne/1488122931412889"><img style="height: 25px;" src="<?php echo site_url();?>/files/social-Facebook-black.svg"></a>
    <a class="col-xs-3" title="Follow us on Instagram" href="https://instagram.com/catcafemelbourne/"><img style="height: 25px;" src="<?php echo site_url();?>/files/social-Instagram-black.svg"></a>
    <a class="col-xs-3" title="Follow us on Tumblr" href="http://catcafemelbourne.tumblr.com/"><img style="height: 25px;" src="<?php echo site_url();?>/files/social-Tumblr-black.svg"></a>
    <a class="col-xs-3" title="Follow us on Twitter" href="https://twitter.com/catcafemelb"><img style="height: 25px;" src="<?php echo site_url();?>/files/social-Twitter-black.svg"></a>
  </p>
</header>

<header class="banner navbar navbar-inverse navbar-static-top" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only"><?= __('Toggle navigation', 'sage'); ?></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a>
    </div>

    <nav class="collapse navbar-collapse" role="navigation">
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'walker' => new wp_bootstrap_navwalker(), 'menu_class' => 'nav navbar-nav']);
      endif;
      ?>
    </nav>
  </div>
</header>


