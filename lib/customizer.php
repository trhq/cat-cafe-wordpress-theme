<?php

namespace Roots\Sage\Customizer;

use Roots\Sage\Assets;

/**
 * Add postMessage support
 */
function customize_register($wp_customize) {
	$wp_customize->get_setting('blogname')->transport = 'postMessage';

}
add_action('customize_register', __NAMESPACE__ . '\\customize_register');

/**
 * Add setting for Cat Cafe customizer
 */
function cat_cafe_color_customizer($wp_customize) {
	$wp_customize->add_section(
		'cc_theme_display_options', [
			'title'    => 'Cafe Colours',
			'priority' => 200
	]);
	$wp_customize->add_setting(
		'cc_link_color', [
			'default'   => '#000000',
			'transport' => 'postMessage'
	]);
	$wp_customize->add_control(
		'cc_link_color', [
			'section' => 'cc_theme_display_options',
			'label'   => 'Link Colour',
			'type'    => 'text'
	]);
	// Heading Colors
	$wp_customize->add_setting(
		'cc_heading_color', [
			'default'   => '#000000',
			'transport' => 'postMessage'
	]);
	$wp_customize->add_control(
		'cc_heading_color', [
			'section' => 'cc_theme_display_options',
			'label'   => 'Link Colour',
			'type'    => 'text'
	]);

}
add_action('customize_register', __NAMESPACE__ . '\\cat_cafe_color_customizer');

function cat_cafe_customizer_css() {
	?>
	<style>
		a {
			color: <?php echo get_theme_mod( 'cc_link_color' ); ?>;
		}
		h1 {
			color: <?php echo get_theme_mod('cc_heading_color'); ?>;
		}
	</style>
	<?php
}
add_action('wp_head', __NAMESPACE__ . '\\cat_cafe_customizer_css');
/**
 * Customizer JS
 */
function customize_preview_js() {
  wp_enqueue_script('wp_cat_cafe/customizer', Assets\asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
}
add_action('customize_preview_init', __NAMESPACE__ . '\\customize_preview_js');
