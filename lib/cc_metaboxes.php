<?php
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB2 directory)
 *
 * @category wp_cat_cafe
 * @package  cc_CMB2
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/WebDevStudios/CMB2
 */

/**
 * Get the bootstrap! If using the plugin from wordpress.org, REMOVE THIS!
 */

if ( file_exists( dirname( __FILE__ ) . '/cmb2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/cmb2/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/CMB2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/CMB2/init.php';
}

add_action( 'cmb2_admin_init', 'cc_register_page_metabox' );
function cc_register_page_metabox() {
	$prefix = 'cc_page_';

	$cmb_page = new_cmb2_box([ 
		'id' => $prefix.'metabox',
		'title' => __( 'Test Metabox', 'cmb2'),
		'object_types' => ['page'],
		'show_name' => true,
	]);
	$cmb_page->add_field( array(
		'name'       => __( 'Test Text', 'cmb2' ),
		'desc'       => __( 'field description (optional)', 'cmb2' ),
		'id'         => $prefix . 'text',
		'type'       => 'text',
		// 'show_on_cb' => 'cc_hide_if_no_cats', // function should return a bool value
		// 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
		// 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
		// 'on_front'        => false, // Optionally designate a field to wp-admin only
		// 'repeatable'      => true,
	) );

	$cmb_page->add_field( array(
		'name' => __( 'Test Image', 'cmb2' ),
		'desc' => __( 'Upload an image or enter a URL.', 'cmb2' ),
		'id'   => $prefix . 'image',
		'type' => 'file',
	) );
}
/**
 * Hook in and add a metabox for adding slides.
 */
add_action( 'cmb2_admin_init', 'cc_register_repeatable_slideshow_metabox' );
function cc_register_repeatable_slideshow_metabox() {
	$prefix = 'cc_slideshow_';

	/**
	 * Repeatable Field Groups
	 */
	$cmb_group = new_cmb2_box( array(
		'id'           => $prefix . 'metabox',
		'title'        => __( 'Testimonial Slide Show', 'cmb2' ),
		'object_types' => array( 'page', ),
		 'show_on'      => array( 'key' => 'page-template', 'value' => 'template-home.php' ),
	) );

	/**
	 * A repeatable grouped field for creating slides.
	 */
	$group_field_id = $cmb_group->add_field( array(
		'id'          => $prefix . 'testimonial',
		'type'        => 'group',
		'description' => __( 'Add a customer testimonial which will be shown on the front page of the website.<br><br>Add the customer\'s name, location and add an image to be shown with their tesitmonial.', 'cmb2' ),
		'options'     => array(
			'group_title'   => __( 'Testimonial Slide {#}', 'cmb2' ), // {#} gets replaced by row number
			'add_button'    => __( 'Add Another  Slide', 'cmb2' ),
			'remove_button' => __( 'Remove Slide', 'cmb2' ),
			'sortable'      => true, // beta
			'closed'     => true, // true to have the groups closed by default
		),
	) );

	/** Slide Image */
	$cmb_group->add_group_field( $group_field_id, array(
		'name' => __( 'Slide Image', 'cmb2' ),
		'id'   => 'image',
		'description'   => 'Add an image to be shown with this testimonial.',
		'type' => 'file',
	) );
	/** Customer Name */
	$cmb_group->add_group_field( $group_field_id, array(
		'name'       => __( 'Customer', 'cmb2' ),
		'id'         => 'name',
		'description'   => 'Add the customer\'s name.',
		'type'       => 'text',
	) );
	/** Customer Location */
	$cmb_group->add_group_field( $group_field_id, array(
		'name'       => __( 'Location', 'cmb2' ),
		'id'         => 'location',
		'description'   => 'Add the customer\'s location.',
		'type'       => 'text',
	) );
	/** Customer Testimonial */
	$cmb_group->add_group_field( $group_field_id, array(
		'name'        => __( 'Testimonial', 'cmb2' ),
		'description' => __( 'Enter a short customer testimonial', 'cmb2' ),
		'id'          => 'testimonial',
		'type'        => 'textarea_small',
	) );
}
