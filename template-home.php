<?php
/**
 * Template Name: Home Page
 */
?>
<?php while (have_posts()) : the_post(); ?>
	<?php $slides = get_post_meta( get_the_id(), 'cc_slideshow_testimonial' ); ?>
	<ul id="testimonial-slideshow">
		<?php foreach($slides[0] as $slide):  ?>
			<li> 
				<span class="slide-text">
					<span class= "testimonial"> 	
						&ldquo;<?php echo $slide['testimonial']; ?>&rdquo; 
					</span>
					<?php echo $slide['name']; ?>, 
					<?php echo $slide['location']; ?> 
				</span>
				<a href="#" class="btn btn-warning">Book <span class="visible-xs-inline">now</span><span class="hidden-xs">to see the cats</span></a>
				<img src="<?php echo $slide['image']; ?>">
			</li>
		<?php endforeach; ?>
	</ul>
	<script>
	jQuery(document).ready(function(){
	var $ = jQuery,
		$slideShow = $('#testimonial-slideshow'),
		$slideText = $slideShow.find('.slide-text');
		$slideBtn = $slideShow.find('.btn');;
	var height;	
	var length = $slideShow.children().length - 1,
		counter = 0; 
	
	/**
	 * Checks the curent slide's computed height against 
	 * the SlideShow container and adjusts if the screen
	 * has been resized.
	 * @return {void} 
	 */
	function resizeSlideShow() {
		height = $slideShow.children().eq(counter).height();
		if (height !== $slideShow.height()) {
			$slideShow.height(height);		
		}
	}
	

	var resizeTimer; // Timer for delaying resize event
	/**
	 * Sets a timeout and calls resizeSlideShow when the
	 * timer completes.
	 * @return {void}
	 */
	$(window).bind('resize', function(){
		resizeTimer = setTimeout(resizeSlideShow, 250);
	});	
	
	/**
	 * Shows and hides the current slide, then calls
	 * self on the next slide in the slideshow.
	 * @return {void}
	 */
	function slideShow() {
		resizeSlideShow();
		
		// Send all slides to back
		$slideShow.children().css('z-index', 1);
		
		// Bring current slide to front and display.
		$slideShow.children().eq(counter)
		    .css('z-index', 2)
		    .fadeIn(1500)
		    .delay(4500)  
		    .fadeOut(500);

		// Display the slide text
		$slideText.hide();
		$slideText.eq(counter)
			.delay(500)
			.slideDown(1000);

		// Display the slide button
		$slideBtn.hide();
		$slideBtn.eq(counter)
			.delay(500)
			.show(750);
		// Increment and call self.
		counter = (counter < length) ? counter + 1 : 0;
		setTimeout(slideShow, 5400); 
	}

	slideShow();
	});
	</script>
	<article class="home">
		<?php get_template_part('templates/page', 'header'); ?>
		<?php get_template_part('templates/content', 'page'); ?>
	</article>
<?php endwhile; ?>

