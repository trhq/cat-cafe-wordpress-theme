(function($) {
	// Site title
	wp.customize('blogname', function(value) {
		value.bind(function(to) {
			$('.brand').text(to);
		});
	});

	// Link color
	wp.customize('cc_link_color', function(value) {
		value.bind(function(to) {
			$('a').css('color', to);
		});
	});

	// Link color
	wp.customize('cc_heading_color', function(value) {
		value.bind(function(to) {
			$('.page-header').css('color', to);
		});
	});

  

})(jQuery);
